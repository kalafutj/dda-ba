#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::fastNegative()
{
	int x = image.height();
	int y = image.width();
	unsigned __int64 asd = 0x00ffffff00ffffff;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		QRgb* scanline=(QRgb*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			scanline[j] = 16777215 - scanline[j];
		}
		/*unsigned __int64* scanline=(unsigned __int64*)image.scanLine(i);
		for (int j = 0; j < y/2; j++)
		{
			scanline[j] ^= asd;
		}*/
	}
	update();
}

void PaintWidget::blackWhite()
{

	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			uchar color = (scanline[j * 4 + 0] * 0.11) + (scanline[j * 4 + 1] * 0.59) + (scanline[j * 4 + 2] * 0.3);
			scanline[j * 4 + 0] = color;
			scanline[j * 4 + 1] = color;
			scanline[j * 4 + 2] = color;
		}
	}
	/*for (int i = 0; i < image.width(); i++)
	{
		for (int j = 0; j < image.height(); j++)
		{
			QColor farba = image.pixelColor(i, j);
			farba.setRed(255 - farba.red());
			farba.setBlue(255 - farba.blue());
			farba.setGreen(255 - farba.green());
			image.setPixelColor(i, j, farba);
		}
	}*/
	update();
}

void PaintWidget::sepiaTone()
{
	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			int colorR = scanline[j * 4 + 2] * 0.393 + scanline[j * 4 + 1] * 0.769 + scanline[j * 4 + 0] * 0.189;
			int colorG = scanline[j * 4 + 2] * 0.349 + scanline[j * 4 + 1] * 0.686 + scanline[j * 4 + 0] * 0.168;
			int colorB = scanline[j * 4 + 2] * 0.272 + scanline[j * 4 + 1] * 0.534 + scanline[j * 4 + 0] * 0.131;
			scanline[j * 4 + 2] = colorR > 255 ? 255 : colorR;
			scanline[j * 4 + 1] = colorG > 255 ? 255 : colorG;
			scanline[j * 4 + 0] = colorB > 255 ? 255 : colorB;
		}
	}
	/*
	outputRed = (inputRed * .393) + (inputGreen *.769) + (inputBlue * .189)
outputGreen = (inputRed * .349) + (inputGreen *.686) + (inputBlue * .168)
outputBlue = (inputRed * .272) + (inputGreen *.534) + (inputBlue * .131)*/
	/*for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			QRgb farba = image.pixel(j, i);
			image.setPixel(j, i, 16777215 - farba);
		}
	}*/
	update();
}

void PaintWidget::medianFilter()
{
	
	int x = image.height();
	int y = image.width();
	uchar *data = image.bits();
	int riadok = image.bytesPerLine();
#pragma omp parallel
	{
		int reds[9];
		int greens[9];
		int blues[9];
#pragma omp for
		for (int i = 1; i < x - 1; i++)
		{
			for (int j = 1; j < y - 1; j++)
			{
				int counter = 0;
				for (int k = -1; k < 2; k++)
					for (int l = -1; l < 2; l++)
					{
						blues[counter] = data[(i + k)*riadok + (j + l) * 4 + 0];
						greens[counter] = data[(i + k)*riadok + (j + l) * 4 + 1];
						reds[counter] = data[(i + k)*riadok + (j + l) * 4 + 2];
						counter++;
					}
				data[i*riadok + j * 4 + 0] = blues[selectKth(blues, 0, 9, 4)];
				data[i*riadok + j * 4 + 1] = greens[selectKth(greens, 0, 9, 4)];
				data[i*riadok + j * 4 + 2] = reds[selectKth(reds, 0, 9, 4)];
			}
		}
	}
}

void PaintWidget::saltPepper()
{
	int count = image.width()*image.height()*0.1;
	QRgb *data = (QRgb *)image.bits();
	int riadok = image.bytesPerLine()/4;
#pragma omp parallel
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> xsur(0, image.width() - 1);
		std::uniform_int_distribution<> ysur(0, image.height() - 1);
#pragma omp for
		for (int i = 0; i < count; i++)
		{
			data[ysur(gen)*riadok +xsur(gen)] = i % 2 == 0 ? 0x00000000 : 0x00ffffff;
		}
	}
}

void PaintWidget::RotateLeft()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine()/4;
	int riadok2 = druhy.bytesPerLine()/4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[i + (y-j-1)*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::RotateRight()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine() / 4;
	int riadok2 = druhy.bytesPerLine() / 4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[x - i - 1 + j*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

//funkcia na vypocet bodov kruznice a volanie dda(ak priamka) alebo ba
void PaintWidget::kruznica(int r, int n, bool priamka) {
	int X = 400;
	int Y = 400+r;
	int x, y;
	if (priamka) for (int i = 1;i <= n;i++) {
			x = X;
			y = Y;
			X = 400 + (int)(r*sin(i * 2 * M_PI / n));
			Y = 400 + (int)(r*cos(i * 2 * M_PI / n));
			dda(x, y, X, Y);
		}
	else for (int i = 1;i <= n;i++) {
		x = X;
		y = Y;
		X = 400 + (int)(r*sin(i * 2 * M_PI / n));
		Y = 400 + (int)(r*cos(i * 2 * M_PI / n));
		bresenham(x, y, X, Y);
	}
}

void PaintWidget::dda(int zx, int zy, int kx, int ky) {

	if (abs(ky - zy) > abs(kx - zx)) {
		//vypocet pre smernicu >1
		double k = (kx - zx) / (double)(ky - zy);
		double q = kx - k*ky;
		//ci su zaciatocne body urcene zhora nadol alebo naopak
		if (zy > ky) for (int y = zy; y > ky;y--) image.setPixelColor(k*y + q, y, myPenColor);
		else for (int y = zy;y < ky;y++) image.setPixelColor(k*y + q, y, myPenColor);
	}
	else {
		//vypocet pre smernicu <1
		double k = (ky - zy) / (double)(kx - zx);
		double q = ky - k*kx;
		// ci su zaciatocne body urcene zprava dolava alebo naopak
		if (zx > kx)for (int x = zx;x > kx;x--) image.setPixelColor(x, k*x + q, myPenColor);
		else for (int x = zx;x < kx;x++) image.setPixelColor(x, k*x + q, myPenColor);
	}
}

void PaintWidget::bresenham(int zx, int zy, int kx, int ky) {
	bool var,suhlas;
	int sign, iter,dx,dy;
	dx = kx - zx;
	dy = ky - zy;
	//rozhodnutie ci bude iterovat cez x alebo y
	if (abs(dx) > abs(dy)) var = true;
	else var = false;

	//rozhodnutie ci sa jedna o spravne orientovanu (lavo-pravu alebo dol-hornu usecku)
	if (var) if (dx > 0)iter = 1;
			 else iter = -1;
	else if (dy > 0) iter = 1;
		 else iter = -1;

	//znamienko rastu/poklesu vzhladom na orientaciu pociatocnych/koncovych bodov a orientaciu
	if (dx>0&&dy<0) if(var) sign = -1; else sign = 1;
	if (dx<0&&dy>0) if(var) sign = 1; else sign = -1;
	if (dx>0&&dy>0) sign = 1;
	if (dx<0&&dy<0) sign = -1;

	//rozhodnutie, ci sa jedna o body so sucastnym rastom/poklesom x aj y hodnoty
	if ((dx>=0&&dy>=0) || (dx<=0&&dy<=0)) suhlas = true;
	else suhlas = false;

	if (var) ba(zx, zy, kx, var, dx, dy, iter, sign,suhlas);
	else ba(zy, zx, ky, var, dy, dx, iter, sign,suhlas);
}

//z - urcuje pociatocny bod
//f - prisluchajuca zavisla hodnota v bode z
//k - koncovy bod
//xy - rozhodnutie ci iterujeme cez x alebo y
//dx - rozsah nezavislej premennej
//dy - rozsah zavislej premennej
//xx - iteracna konstanta
//yy - potencialna zmena zavislej premennej
//suhlas - body so zapornou smernicou
void PaintWidget::ba(int z, int f, int k, bool xy, int dx, int dy, int xx, int yy,bool suhlas) {
	int k1, k2, p;
	//body so zapornou smernicou maju iny vzorec na vypocet {k1,k2,p}
	if (suhlas) {
		k1 = 2 * dy;
		k2 = 2 * dy - 2 * dx;
		p = 2 * dy - dx;
		p *= xx;
		k1 *= xx;
		k2 *= xx;}
	else {
		k1 = 2 * dy;
		k2 = 2 * dy + 2 * dx;
		p = 2 * dy + dx;
		p *= yy;
		k1 *= yy;
		k2 *= yy;
	}
	//samotny priebeh vypoctu
	for (int x = z;x != k;x += xx) {
		if (xy) image.setPixelColor(x, f, myPenColor);
		else image.setPixelColor(f, x, myPenColor);
		if (p > 0) { f += yy; p += k2; }
		else p += k1;
	}
}